package br.com.mastertech.imersivo.filmesdb.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "filmes")

public class Filme {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column
	private Long id;
	
	@Column
	private String titulo;
	
	@Column
	private String genero;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
		

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}
}
