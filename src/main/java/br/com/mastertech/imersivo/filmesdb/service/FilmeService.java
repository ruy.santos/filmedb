package br.com.mastertech.imersivo.filmesdb.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.repository.FilmeRepository;

@Service
public class FilmeService {
	
	@Autowired
	private FilmeRepository filmerepository;

	public Iterable<Filme> obterFilmes() {
		System.out.println("Chamaram o listar filmes");
		return filmerepository.findAll();
	}

	public void criarFilme(Filme filme) {
		filmerepository.save(filme);
		System.out.println("Chamaram o criadro do " + filme.getTitulo());

	}
}
