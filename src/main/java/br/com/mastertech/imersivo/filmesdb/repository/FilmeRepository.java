package br.com.mastertech.imersivo.filmesdb.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.mastertech.imersivo.filmesdb.model.Filme;


	
@Repository
public interface FilmeRepository extends CrudRepository<Filme, Long>{

}