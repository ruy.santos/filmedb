package br.com.mastertech.imersivo.filmesdb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.service.FilmeService;

@RestController
public class FilmeController {

	@Autowired
	private FilmeService filmeService;
	
	@GetMapping("/filme")
	public Iterable<Filme> listarFilmes() {
		return filmeService.obterFilmes();
	}

	@PostMapping("/filme")
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarFilme(@RequestBody Filme filme) {
		filmeService.criarFilme(filme);
	}
}
